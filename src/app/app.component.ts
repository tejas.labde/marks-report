import { DataService } from '../services/data.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'evalproj';

  fullCount:number=0;
  partialCount:number=0;
  noneCount:number=0;
  studentScore:number=0;
  subjectCount:number=0;
  countVal:number=0;

  constructor(private dataService:DataService){}

  ngOnInit():void{
    this.getData();
  }

  getData(){
    this.dataService.getData().subscribe((resp:any)=>{
      resp.data.map((student:any)=>{
        this.studentScore=0;
        this.subjectCount=0;
       
        student.marksInfo.map((marksObj:any) => {
          this.subjectCount+=1;
          this.studentScore+=Number(marksObj.obtainedMarks);
        })
       
        this.countVal=this.studentScore/this.subjectCount;
        if(this.countVal==1){ this.fullCount+=1;}
        else if(this.countVal==0){this.noneCount+=1;}
        else { this.partialCount+=1;}
       
      })
    })
   
  }
}

